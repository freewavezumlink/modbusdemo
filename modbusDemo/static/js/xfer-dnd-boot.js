jQuery(document).ready(function($) {
	// jQuery 1.x creates it's own event object, and it doesn't have a
	// dataTransfer property yet. This adds dataTransfer to the event object.
	jQuery.event.props.push('dataTransfer');
	
	var internalDNDType = 'text/html'; 
	var $fileList = $('#file-list');
	var $columns = $(".column");
	
	$fileList.on('dragstart', function(e) {
		var target = $( event.target );
		if ( target.is( "li" ) ) {
	        // use the element's data-value="" attribute as the value to be copying:
	        e.dataTransfer.setData(internalDNDType, e.target.dataset.value);
	        e.dataTransfer.effectAllowed = 'move'; // only allow copies
	    } else {
	    	event.preventDefault(); // don't allow selection to be dragged
	    }
	});
	$('input[type="radio"]').change(function() {
		// Write a custom javascript class so we can actually disable the slider
		if($(this).is(":checked")) {
			var val = $(this).val();
			if(val == 'apply') {
				$("#xferRate").prop("disabled", true);
				$("#xfer-div").css("background-color", "#ebebeb");
			} else {
				$("#xferRate").prop("disabled", false);
				$("#xfer-div").css("background-color", "");
			}
		}
	});

	$columns.each(function() {
		$(this).on('dragenter dragover drop', function(e) {
			// preventing the unwanted behaviors
			e.preventDefault();
			e.stopPropagation();
		})
		.on('dragenter', function(e) {
			// report whether or not the drop target is to accept the drop
			e.dataTransfer.dropEffect = 'copy'; // explicitly show this as a copy
		})
		.on('dragover dragenter', function(e) {
			$(this).addClass('column-dragover');
		})
		.on('dragleave dragend drop', function() {
			$(this).removeClass('column-dragover');
		})
		.on('drop', function(e) {
			$li = showFileForTransfer(e);
			var $header = $("header", this);
			var filename = e.dataTransfer.getData(internalDNDType);
			var val = $('input[name="xferOrApply"]:checked', '#controlsForm').val();
			if(val == 'xfer') {
				var sliderData = $('#xferRate').slider().data();
				var sliderValue = sliderData.value;
				if(sliderValue === undefined) {
					sliderValue = sliderData.sliderValue;
				}
				var json = {
						to: $(".panel-title", this).html(),
						xferRate: sliderValue,
						payload: filename
					};
				var jsonString = JSON.stringify(json);
				triggerRequestFileTransfer($li, jsonString);
			} else {
				triggerRequestApplyFile(
					$li,
					JSON.stringify({
							file_name: filename,
							client_id: $(".panel-title", this).html()
					})
				);
			}
		});
	});
		
	var showFileForTransfer = function(e) {
		var $div = $(e.currentTarget).find(".panel-body");
		
		var $ul = $div.find(".selectable");
		if(!$ul.exists()) {
			$ul = $("<ul/>");
			$ul.addClass("selectable")
			   .addClass("list-group");
			$div.append($ul);
		}
		var $li = $ul.find("li", "#" + data);
		if(!$li.exists()) {
			$li = $("<li/>");
		    var data = e.dataTransfer.getData(internalDNDType);
		    $li.text(data)
		    	.attr("id", data)
			    .attr('data-toggle', 'tooltip')
			    .attr('data-placement', 'top');
		}
		$li.addClass("in-progress")
		   .addClass("list-group-item");
		$ul.append($li).hide().slideDown();

		return $li;
	};


	var provideFeedback = function(feedback) {
		$("#feedback")
			.text(feedback)
			.addClass("feedback-error")
			.slideDown()
			.delay(5000)
			.slideUp()
			;
		window.setTimeout(function() {
			$("#feedback").text('');
		}, 5000);
	};

	
	var triggerRequestFileTransfer = function($li, jsonString) {
		$.ajax({
			type: 'post',
			url: '/api/requestFileTransfer',
			dataType: 'json',			// what data type we are expecting returned
			cache: false,				// don't cache
			contentType: "application/json; charset=utf-8",
			data: jsonString,			// use passed in data
			processData: false,			// don't process the data
			statusCode: {
				200: function(response) {
					if($li.exists()) {
						$li.removeClass("in-progress")
						   .addClass("success");
						var title = "";
						$.each(response, function(key, val) {
							title = title + key + ": " + val + ", ";
						});
						$li.prop("title", title)
 						   .tooltip();

					}
				},
				400: function(e) {
					if($li.exists()) {
						$li.removeClass("in-progress")
						   .addClass("failure")
						   .prop("title", "submitted data is invalid")
						   .tooltip();
					}
					provideFeedback(e.responseText);
				},
				500: function(e) {
					if($li.exists()) {
						$li.removeClass("in-progress")
						   .addClass("failure")
						   .prop("title", "server was unable to complete request")
						   .tooltip();
					}
					provideFeedback(e.responseText);
				}
			}
		});
	};

	
	var triggerRequestApplyFile = function($li, param) {
		$.ajax({
			type: 'put',
			url: '/api/requestApplyFile',
			dataType: 'json',			// what data type we are expecting returned
			cache: false,				// don't cache
			contentType: "application/json; charset=utf-8",
			data: param,				// use passed in data
			processData: false,			// don't process the data
			statusCode: {
				200: function(response) {
					if($li.exists()) {
						$li.removeClass("in-progress")
						   .addClass("success")
						   .prop("title", "Successfully applied")
						   .tooltip();
					}
				},
				400: function(e) {
					if($li.exists()) {
						$li.removeClass("in-progress")
						   .addClass("failure")
						   .prop("title", "submitted data is invalid")
						   .tooltip();
					}
					provideFeedback(e.responseText);
				},
				500: function(e) {
					if($li.exists()) {
						$li.removeClass("in-progress")
						   .addClass("failure")
						   .prop("title", "server was unable to complete request")
						   .tooltip();
					}
					provideFeedback(e.responseText);
				}
			}
		});
	};

});
