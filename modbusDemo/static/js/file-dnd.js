jQuery( document ).ready(function( $ ) {

/*****
	jQuery 1.x creates it's own event object, and it doesn't have a
	dataTransfer property yet. This adds dataTransfer to the event object.
*****/
jQuery.event.props.push('dataTransfer');

/*Feature support detection for DND and advanced uploading*/
var isDNDUpload = function() {
	var div = document.createElement('div');
	return (('draggable' in div) || ('ondragstart' in div && 'ondrop' in div)) 
		&& 'FormData' in window && 'FileReader' in window;
}();
	
	
var $example = $('#fileUploadForm');
var droppedFiles = false;
var input = $('input#file.file-input');
var showFiles = function(files) {
	if(files) {
		var output = [];
		$.each(files, function(i, file) {
			output.push(
				'<li draggable="true" data-value="', escape(file.name), '">',
					'<strong>', escape(file.name), '</strong> (',
					file.type || 'n/a', 
					') - ', file.size, ' bytes, last modified: ',
					file.lastModifiedDate ? 
						file.lastModifiedDate.toLocaleDateString() : 'n/a', 
				'</li>');
		});
		$('#file-list').append(output.join(''));
	}
};

var showFile = function(file) {
	var $li;
	if(file) {
		var $file_list = $('#file-list');
		if($file_list.has("li").length == 0) {
			$file_list.css('margin-top', '15px');
		}
		$li = $file_list.find("[data-value='" + file.name + "']");
		if(!$li.exists()) {
			$li = $("<li/>");
			var filename = "<strong>" + escape(file.name) + "</strong>";
			var filetype = " (" + (file.type || "n/a") + ")";
			var filesize = " - " + file.size + " bytes";
			var lastmod = ", last modified: " + (file.lastModifiedDate 
				? file.lastModifiedDate.toLocaleDateString()
				: 'n/a');
			var content = filename + filetype + filesize + lastmod;
			$li.html(content)
			   .addClass('in-progress')
			   .attr('draggable', 'true')
			   .attr('data-value', escape(file.name))
//   			   .attr('data-toggle', 'tooltip')
//			   .attr('data-placement', 'left');
			   ;

			$file_list.append($li).hide().slideDown();
		}
	}
	return $li;
};


var showProgress = function(e) {
	if(e.lengthComputable) {
		var percentComplete = parseInt((e.loaded / e.total) * 100);
		$("#progressbar").progressbar("value", percentComplete);
	}
};

	
$example.on('drag dragstart dragend dragover dragenter dragleave drop submit', function(e) {
		// preventing the unwanted behaviors
		e.preventDefault();
		e.stopPropagation();
	})
	.on('dragover dragenter', function(e) {
		$example.addClass('is-dragover');
		e.dataTransfer.dropEffect = 'copy'; // explicitly show this as a copy
	})
	.on('dragleave dragend drop', function() {
		$example.removeClass('is-dragover');
	})
	.on('drop', function(e) {
		droppedFiles = e.dataTransfer.files; // the files that were dropped
		var $li = showFile(droppedFiles[0]);
		var formData = new FormData();
		formData.append('payload', droppedFiles[0]);
		triggerFileUpload($li, formData);
	})
	;

var triggerFileUpload = function($li, formData) {
	// ajax request
	$.ajax({
		type: $example.attr('method'),
		url: $example.attr('action'),
		cache: false,				// don't cache
		contentType: false,			// don't worry about the content type
		data: formData,				// use data we created
		processData: false,			// don't process the data
		xhr: function() {
			myxhr = $.ajaxSettings.xhr();
			if(myxhr.upload) {
				$("#progressbar").progressbar().show();	
				myxhr.upload.addEventListener('progress', showProgress, false);
			} else {
				$("#progressbar").progressbar("value", false).show();
				console.log("Upload progress is not supported in this browser");
			}
			return myxhr;
		},
		complete: function(e) {
			$("#progressbar").hide();
		},
		statusCode: {
			200: function() {
				$("#xferBtn").prop("disabled", false);
				if($li.exists()) {
					$li.removeClass("in-progress")
					   .addClass("success")
//					   .prop("title", "successfully uploaded")
//					   .tooltip();
					   ;
				}
			},
			409: function(e) {
				if($li.exists()) {
					$li.removeClass("in-progress")
					   .addClass("failure")
					   .prop("title", "this file failed security check")
					   .tooltip();
				}
				provideFeedback(e.responseText);
			},
			404: function(e) {
				if($li.exists()) {
					$li.removeClass("in-progress")
					   .addClass("failure")
					   .prop("title", "filename is invalid")
					   .tooltip();
				}
				provideFeedback(e.responseText);
			},
			500: function(e) {
				if($li.exists()) {
					$li.removeClass("in-progress")
					   .addClass("failure")
					   .prop("title", "post request data was invalid")
					   .tooltip();
				}
				provideFeedback(e.responseText);
			}
		},
		error: function(e) {
			if($li.exists()) {
				$li.removeClass("in-progress")
				.addClass("failure")
				.prop("title", e.statusText)
				.tooltip();
			}
		}
	});
}


var provideFeedback = function(feedback, iserror) {
	if(iserror) {
		$("#feedback").addClass("feedback-error");
	}
	$("#feedback")
		.text(feedback)
		.slideDown()
		.delay(5000)
		.slideUp()
		;
	window.setTimeout(function() {
		$("#feedback").text('');
		if(iserror) {
			$('#feedback').removeClass("feedback-error");
		}
	}, 5000);
};

});
