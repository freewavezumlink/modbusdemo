jQuery(document).ready(function($) {
/*
	$(document).ready(function (){
		$("html").ajaxStart(function() { $(this).addClass("wait"); });
		$("html").ajaxStop(function() { $(this).removeClass("wait"); });
	});
*/
	
	$('#refreshDiscovery').on('click', function(e) {
		// preventing the unwanted behaviors
		e.preventDefault();
		e.stopPropagation();
		$.ajax({
			url: '/api/refreshDiscovery',
			type: 'GET',
			dataType: 'json',			// what data type we are expecting returned
			success: refreshDiscovery,
			beforeSend: ajaxStart,
			complete: ajaxStop
	    });
	});
	
	function ajaxStart() {
		$("html").addClass("wait");
		$('#refreshDiscovery').prop("disabled",true);
	}
	
	function ajaxStop() {
		$("html").removeClass("wait");
		$('#refreshDiscovery').prop("disabled",false);
	}
	
	function refreshDiscovery(data) {
		var $columns = $("#columns");
		$columns.empty();
		$.each(data, function(device, props) {
			var $div = $("<div/>");
			$columns.append($div);
			$div.addClass("column");
			var $header = $("<header/>");
			$div.append($header);
			$header.addClass("bg-primary")
			       .text(device);
			$ul = $("<ul/>");
			$div.append($ul);
			// <li>SN: {{ value.serial_number }}</li>
			$li_sn = $("<li/>");
			$li_sn.text('SN: ' + props.serial_number);
			$ul.append($li_sn);
			
			// <li>Name: {{value.device_name}}</li>
			$li_name = $("<li/>");
			$li_name.text('Name: ' + props.device_name);
			$ul.append($li_name);
			
			//<li>MAC: {{value.mac_address}}</li>
			$li_mac = $("<li/>");
			$li_mac.text('MAC: ' + props.mac_address);
			$ul.append($li_mac);
			
			// <li>Device Ver: {{value.device_firmware_version}}</li>
			$li_dev = $("<li/>");
			$li_dev.text('Device Ver: ' + props.device_firmware_version);
			$ul.append($li_dev);
			
			// <li>Radio Ver: {{value.radio_firmware_version}}</li>
			$li_radio = $("<li/>");
			$li_radio.text('Radio Ver: ' + props.radio_firmware_version);
			$ul.append($li_radio);
		});
		$columns.applyDragAndDropToDevices();

	}

	

});
