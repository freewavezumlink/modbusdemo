jQuery(document).ready(function($) {
	// jQuery 1.x creates it's own event object, and it doesn't have a
	// dataTransfer property yet. This adds dataTransfer to the event object.
	jQuery.event.props.push('dataTransfer');
	
	
	jQuery.fn.applyDragAndDropToDevices = function() {
		$(".column").each(function() {
			$(this).on('drag dragstart dragend dragover dragenter dragleave drop submit', function(e) {
				// preventing the unwanted behaviors
				e.preventDefault();
				e.stopPropagation();
			})
			.on('dragover dragenter', function(e) {
				// report whether or not the drop target is to accept the drop
				e.dataTransfer.dropEffect = 'copy'; // explicitly show this as a copy
				$(this).addClass('column-dragover');
			})
			.on('dragleave dragend drop', function() {
				$(this).removeClass('column-dragover');
			})
			.on('drop', function(e) {
				var $li = showFile(e);
				var $header = $("header", this);
				var filename = e.dataTransfer.getData(internalDNDType);

				var val = $('input[name="xferOrApply"]:checked', '#controlsForm').val();
				if(val == 'xfer') {
					var sliderData = $('#xferRate').slider().data();
					var sliderValue = sliderData.value;
					if(sliderValue === undefined) {
						sliderValue = sliderData.sliderValue;
					}
					triggerRequestFileTransfer(
						$li,
						JSON.stringify({
							to: $("header", this).html(),
							xferRate: sliderValue,
							payload: filename
						})
					);
				} else {
					triggerRequestApplyFile(
						$li,
						JSON.stringify({
								file_name: filename,
								client_id: $("header", this).html()
						})
					);
				}
			});
		});
	}

	
	
	var internalDNDType = 'text/html'; 
	var $fileList = $('#file-list');
	
	$('#columns').applyDragAndDropToDevices();
	
	$fileList.on('dragstart', function(e) {
		var target = $( e.target );
		if ( target.is( "li" ) ) {
	        // use the element's data-value="" attribute as the value to be copying:
	        e.dataTransfer.setData(internalDNDType, e.target.dataset.value);
	        e.dataTransfer.effectAllowed = 'copy'; // only allow copies
	    } else {
	    	event.preventDefault(); // don't allow selection to be dragged
	    }
	});
	$('input[type="radio"]').change(function() {
		if($(this).is(":checked")) {
			var val = $(this).val();
			if(val == 'apply') {
				$("#xferRate").prop("disabled", true);
				$("#xfer-div").css("background-color", "#ebebeb");
			} else {
				$("#xferRate").prop("disabled", false);
				$("#xfer-div").css("background-color", "");
			}
		}
	});
	
	var showFile = function(e) {
		var $div = $(e.currentTarget);
		var $ul = $div.find(".selectable");
		if(!$ul.exists()) {
			$ul = $("<ul/>");
			$ul.attr("class", "selectable");
			$div.append($ul);
		}
		var $li = $("<li/>");
	    var data = e.dataTransfer.getData(internalDNDType);
		$li.text(data)
		   .addClass('in-progress')
		   .attr("id", data)
		   .attr('data-toggle', 'tooltip')
		   .attr('data-placement', 'top');

		$ul.append($li)
			.hide()
			.slideDown();
		return $li;
	};
	
	
	var provideFeedback = function(feedback) {
		$("#feedback")
			.text(feedback)
			.addClass("feedback-error")
			.slideDown()
			.delay(5000)
			.slideUp()
			;
		window.setTimeout(function() {
			$("#feedback").text('');
		}, 5000);
	};

	
	var triggerRequestFileTransfer = function($li, jsonString) {
		$.ajax({
			type: 'post',
			url: '/api/requestFileTransfer',
			dataType: 'json',			// what data type we are expecting returned
			cache: false,				// don't cache
			contentType: "application/json",
			data: jsonString,			// use passed in data
			processData: false,			// don't process the data
			statusCode: {
				200: function(response) {
					if($li.exists()) {
						$li.removeClass("in-progress")
						   .addClass("success");
						var title = "";
						$.each(response, function(key, val) {
							title = title + key + ": " + val + ", ";
						});
						$li.prop("title", title)
						   .tooltip();
					}
				},
				400: function(e) {
					if($li.exists()) {
						$li.removeClass("in-progress");
						$li.addClass("failure");
						$li.prop("title", "submitted data is invalid")
						   .tooltip();
					}
					provideFeedback(e.responseText);
				},
				500: function(e) {
					if($li.exists()) {
						$li.removeClass("in-progress");
						$li.addClass("failure");
						$li.prop("title", "server was unable to complete request")
						   .tooltip();
					}
					provideFeedback(e.responseText);
				}
			}
		});
	};
	
	var triggerRequestApplyFile = function($li, param) {
		$.ajax({
			type: 'put',
			url: '/api/requestApplyFile',
			dataType: 'json',			// what data type we are expecting returned
			cache: false,				// don't cache
			contentType: "application/json",
			data: param,				// use passed in data
			processData: false,			// don't process the data
			statusCode: {
				200: function(response) {
					if($li.exists()) {
						$li.removeClass("in-progress")
						   .addClass("success")
						   .prop("title", "Successfully applied")
 						   .tooltip();
					}
				},
				400: function(e) {
					if($li.exists()) {
						$li.removeClass("in-progress")
						   .addClass("failure")
						   .prop("title", "submitted data is invalid")
 						   .tooltip();
					}
					provideFeedback(e.responseText);
				},
				500: function(e) {
					if($li.exists()) {
						$li.removeClass("in-progress")
						   .addClass("failure")
						   .prop("title", "server was unable to complete request")
 						   .tooltip();
					}
					provideFeedback(e.responseText);
				}
			}
		});
	};

});
