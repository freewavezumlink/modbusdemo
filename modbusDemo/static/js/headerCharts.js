
var hc_interval = 5000;
var hc_getPrams = ['signalLevel','signalMargin','layerChannel.DownRateAvg','layerChannel.UpRateAvg',
	                'rfStats.DownRateAvg','rfStats.UpRateAvg','RadioLLRx','RadioBadCRC'];

var hc1 = new SmoothieChart({
  millisPerPixel: 1000, scaleSmoothing: 0.065,
  grid: { fillStyle: '#ffffff', strokeStyle: '#000000', millisPerLine: 60000, verticalSections: 5 },
  labels: { fillStyle: '#004080', fontSize: 16, precision: 1 }
});
hc1.streamTo(document.getElementById("hc1"),hc_interval);

// Data
var hc_rssi = new TimeSeries();
var hc_sens = new TimeSeries();

// Add to SmoothieChart
hc1.addTimeSeries(hc_rssi, { lineWidth: 3, strokeStyle: '#00ff00', fillStyle: 'rgba(174,255,174,0.30)' });
hc1.addTimeSeries(hc_sens, { lineWidth: 3, strokeStyle: '#ff0000', fillStyle: 'rgba(255,185,185,0.30)' });

var hc2 = new SmoothieChart({
  millisPerPixel: 1000, scaleSmoothing: 0.065,
  grid: { fillStyle: '#ffffff', strokeStyle: '#000000', millisPerLine: 60000, verticalSections: 5 },
  labels: { fillStyle: '#004080', fontSize: 16, precision: 1 }
});
hc2.streamTo(document.getElementById("hc2"),hc_interval);

// Data
var hc_DownRateAvg = new TimeSeries();
var hc_UpRateAvg = new TimeSeries();
//var hc_TotalRateAvg = new TimeSeries();

// Add to SmoothieChart
hc2.addTimeSeries(hc_DownRateAvg, { lineWidth: 3, strokeStyle: '#00ff00' });
hc2.addTimeSeries(hc_UpRateAvg, { lineWidth: 3, strokeStyle: '#ff0000' });
//hc2.addTimeSeries(hc_TotalRateAvg, { lineWidth: 3, strokeStyle: '#0000ff' });

var hc3 = new SmoothieChart({
  millisPerPixel: 1000, scaleSmoothing: 0.065,
  grid: { fillStyle: '#ffffff', strokeStyle: '#000000', millisPerLine: 60000, verticalSections: 5 },
  labels: { fillStyle: '#004080', fontSize: 16, precision: 1 }
});
hc3.streamTo(document.getElementById("hc3"),hc_interval);

// Data
var hc_per = new TimeSeries();


// Add to SmoothieChart
hc3.addTimeSeries(hc_per, { lineWidth: 3, strokeStyle: '#ff0000', fillStyle: 'rgba(255,185,185,0.30)' });



function hc_parseResult(results) {
		var values = [];
		for (i in results) {
    try {
      var pages = results[i]['RESPONSE']['pages'];
      for (page in pages) {
        for (pram in pages[page]) {
          var value = pages[page][pram];
          var pagePram = page + '.' + pram;
          values[pagePram] = value;
          values[pram] = value;
        }
      }
    } catch (err) { }
		}
  return values;
}

function hc_getData() {
  if (document.getElementById('hc_enabled').checked)
  {
    $.ajax({
      type: 'GET',
      url: '/cli',
      data: hc_getPrams.join('&'),
      dataType: 'json',
      processData: false,
      success: function (result) {
        try {
          var values = hc_parseResult(result);
          //console.log('values');
          //console.log(values);
          hc_sens.append(new Date().getTime(), parseFloat(values['signalLevel']) - parseFloat(values['signalMargin']));
          hc_rssi.append(new Date().getTime(), parseFloat(values['signalLevel']));

          hc_DownRateAvg.append(new Date().getTime(), parseFloat(values['DownRateAvg']));
          hc_UpRateAvg.append(new Date().getTime(), parseFloat(values['UpRateAvg']));
          //hc_TotalRateAvg.append(new Date().getTime(), parseFloat(values['DownRateAvg'])+parseFloat(values['UpRateAvg']));

          var RadioLLRx = parseFloat(values['RadioLLRx']);
          var RadioBadCRC = parseFloat(values['RadioBadCRC']);
          var per = 0;
          if ((RadioLLRx + RadioBadCRC) > 0) {
            per = (RadioBadCRC * 100.0) / (RadioLLRx + RadioBadCRC);
          }

          hc_per.append(new Date().getTime(), per);



        } catch (err) {
        }
      },
      complete: function (data) {
        // Schedule the next
        setTimeout(hc_getData, hc_interval);
      }
    });
  }
  else
  {
    setTimeout(hc_getData, hc_interval);
  }
}

setTimeout(hc_getData, hc_interval);






