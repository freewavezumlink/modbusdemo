jQuery(document).ready(function($) {
	// jQuery 1.x creates it's own event object, and it doesn't have a
	// dataTransfer property yet. This adds dataTransfer to the event object.
	jQuery.event.props.push('dataTransfer');
	
	var internalDNDType = 'text/html'; 

	$("#xferResultsBtn").on('click', function(e) {
		// preventing the unwanted behaviors
		e.preventDefault();
		e.stopPropagation();
		$('#statsModal').modal('show');
	});
	
	$("#xferBtn").on('click', function(e) {
		// preventing the unwanted behaviors
		e.preventDefault();
		e.stopPropagation();
		$filelist = $('#file-list');
		if($filelist.has("li").length == 0) {
			$filelist.provideFeedback("You must upload a file.", true);
		} else {
//			var $selected = $filelist.find("li.ui-selected");
//			if(!$selected.exists()) {
//				$filelist.provideFeedback("You must select a file.", true);
//			} else {
//				if($selected.hasClass("failure")) {
//					$filelist.provideFeedback("Cannot transfer a file that failed upload.", true);
//				} else {
					var sliderData = $('#xferRate').slider().data();
					var sliderValue = sliderData.value;
					if(sliderValue === undefined) {
						sliderValue = sliderData.sliderValue;
					}
					triggerGlobalFileTransfer(
						JSON.stringify({
							xferRate: sliderValue,
//							payload: $selected.attr("data-value")
							payload: $filelist.find("li").last().attr("data-value")
						})
					);
//				}
//			}
		}
	});

/*	
	$('#update_drop_zone').on('dragenter dragover drop', function(e) {
			// preventing the unwanted behaviors
			e.preventDefault();
			e.stopPropagation();
		})
		.on('dragenter', function(e) {
			// report whether or not the drop target is to accept the drop
			e.dataTransfer.dropEffect = 'copy'; // explicitly show this as a copy
		})
		.on('dragover dragenter', function(e) {
			$(this).addClass('column-dragover');
		})
		.on('dragleave dragend drop', function() {
			$(this).removeClass('column-dragover');
		})
		.on('drop', function(e) {
			var filename = e.dataTransfer.getData(internalDNDType);
			if(filename) {
				var sliderData = $('#xferRate').slider().data();
				var sliderValue = sliderData.value;
				if(sliderValue === undefined) {
					sliderValue = sliderData.sliderValue;
				}
				triggerGlobalFileTransfer(
					JSON.stringify({
						xferRate: sliderValue,
						payload: filename
					})
				);
			}
		});
*/
	
	var triggerGlobalFileTransfer = function(params) {
		$.ajax({
			type: 'put',
			url: '/api/requestGlobalFileTransfer',
			cache: false,				// don't cache
			contentType: "application/json", // set the HTTP headers
			data: params,				// use passed in data
			processData: false,			// don't process the data
			beforeSend: function() {
				$("#xferResultsBtn").prop("disabled", true);
				$("html").addClass("wait");
			},
			complete: function() {
				$("#xferResultsBtn").prop("disabled", false);
				$("html").removeClass("wait");
			},
			statusCode: {
				200: function(response) {
					retrieveStatistics();
				},
				400: function(e) {
					$filelist.provideFeedback(e.responseText, true);
				},
				500: function(e) {
					$filelist.provideFeedback(e.responseText, true);
				}
			}
		});
	};
	

	var triggerApplyFileGlobally = function(param) {
		$.ajax({
			type: 'put',
			url: '/api/transferAndApplyFileGlobally',
			dataType: 'json',			// what data type we are expecting returned
			cache: false,				// don't cache
			contentType: "application/json",
			data: param,				// use passed in data
			processData: false,			// don't process the data
			beforeSend: function() {
				$("#xferResultsBtn").prop("disabled", true);
				$("html").addClass("wait");
			},
			complete: function() {
				$("#xferResultsBtn").prop("disabled", false);
				$("html").removeClass("wait");
			},
			statusCode: {
				200: function(response) {
					retrieveStatistics();
				},
				400: function(e) {
					provideFeedback(e.responseText);
				},
				500: function(e) {
					provideFeedback(e.responseText);
				}
			}
		});
	};

	var retrieveStatistics = function() {
		var modal = $('#statsModal').modal();
		modal
			.find('.modal-body')
			.load('/api/getTransferStatistics', function(responseText, textStatus) {
				if(textStatus == 'success' || textStatus == 'notmodified') {
					modal.show();
				}
			});
	};
	
//	var buildStatisticsRow = function() {
//	      <div class="row">
//        	<div class="col-md-6 col-md-offset-3">.col-md-6 .col-md-offset-3</div>
//        </div>
//        <div class="row">
//        	<div class="col-sm-9">
//          	Level 1: .col-sm-9 FILE
//          	<div class="row">
//            		<div class="col-xs-8 col-sm-6">
//              		Level 2: .col-xs-8 .col-sm-6 STAT
//            		</div>
//            		<div class="col-xs-4 col-sm-6">
//              		Level 2: .col-xs-4 .col-sm-6 STAT
//            		</div>
//          	</div>
//        	</div>
//        </div>		
//	};
});