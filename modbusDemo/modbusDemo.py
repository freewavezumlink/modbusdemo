import socket
import sys
import time
import re
import json
import os   
import imp
import uuid
import shutil
import tempfile
import subprocess
import platform
import logging
import types
import jinja2
from flask import (
    Flask, 
    url_for,
    request,
    Response,
    jsonify,
    send_from_directory,
    make_response,
    render_template
)

from logging.handlers import RotatingFileHandler
from fileinput import filename
from werkzeug import secure_filename
from werkzeug.datastructures import FileStorage
from functools import wraps

from modbus.modbus import ModbusClass

     
app = Flask(__name__)

  
@app.route('/get', methods =['GET', 'POST', 'PUT'])
def get():

    for arg in request.args:
        print arg,'=',request.args[arg]

    response = {}
    
    ip = '127.0.0.1'
    if 'ip' in request.args: 
        ip = request.args['ip']
    
    port=5042    
    if 'port' in request.args: 
        port = int(request.args['port'])
        
    unidId=1    
    if 'unidId' in request.args: 
        unidId = int(request.args['unidId'])
        
    address=34    
    if 'address' in request.args: 
        address = int(request.args['address'])
        
    FC=4    
    if 'FC' in request.args: 
        FC = int(request.args['FC'])
        
    name='value'    
    if 'name' in request.args: 
        name = request.args['name']
    
    mb = ModbusClass(address = ip, unitId=unidId, port=port , framer = 'ModbusRtuFramer')
    val = mb.readFloat(address,unidId=unidId,FC=FC)
    response[name]=val
                  
    return json.dumps(response)

   
@app.route('/graph')
@app.route('/')
def graph():     
    return render_template('graph.html')
      
@app.route('/css/<path:filename>')
def css_static(filename):
    return send_from_directory(app.root_path + '/static/css/', filename)

@app.route('/js/<path:filename>')
def js_static(filename):
    return send_from_directory(app.root_path + '/static/js/', filename)

@app.route('/img/<path:filename>')
def img_static(filename):
    return send_from_directory(app.root_path + '/static/img/', filename)

@app.errorhandler(400)
def bad_request(error):
    return make_response(jsonify({'error': 'Bad request'}), 400)

@app.errorhandler(404)
def not_found(error):
    return make_response(jsonify({'error': 'Not found'}), 404)

        
def main():
    app.run(host='0.0.0.0',port=5555,debug=True,threaded=True,use_reloader=True)
#    app.run(host='0.0.0.0',debug=True,threaded=True,use_reloader=True)


if __name__ == '__main__':
    sys.exit(main())
    