from pymodbus.client.sync import ModbusTcpClient
#from pymodbus.client.sync import ModbusTcpClient as ModbusClient
#from pymodbus.client.sync import ModbusUdpClient as ModbusClient
#from pymodbus.client.sync import ModbusSerialClient as ModbusClient

from pymodbus.transaction import ModbusAsciiFramer, ModbusRtuFramer
from pymodbus.constants import Defaults

from pymodbus.pdu import ModbusExceptions
from pymodbus.pdu import ModbusResponse
from pymodbus.pdu import ExceptionResponse

from pymodbus.utilities  import checkCRC

import logging
import logging.handlers as Handlers

import socket
import array
import binascii
import struct
import datetime
import sys
import argparse
import re
import time



#clientAddress = '192.168.1.90'
#clientAddress = '10.2.4.15'
#clientAddress = '127.0.0.1'
clientAddress = '10.200.4.101'

logging.basicConfig()
log = logging.getLogger()
log.setLevel(logging.DEBUG)


class ModbusClient(ModbusTcpClient):

    def checkFrame(self,buffer):
        try:
            frame_size = len(buffer)
            data = buffer[:frame_size - 2]
            crc = buffer[frame_size - 2:frame_size]
            crc_val = (ord(crc[0]) << 8) + ord(crc[1])
            return checkCRC(data, crc_val)
        except (IndexError, KeyError):
            return False

    def _recv(self, size):
        response = b''
        
        if not self.socket:
            raise ConnectionException(self.__str__())
        
        try:            
            responseComplete = False
            endTime = time.time() + 1.0

            while not responseComplete:
                self.socket.settimeout(0.025)
                recvData = ''
                try:
                   recvData = self.socket.recv(size)
                except socket.error:
                    pass
                response += recvData
                if len(response) >= 4 and self.checkFrame(response):
                    responseComplete = True
                if time.time() > endTime:
                    break
            
        except socket.error:
            response =  b''
        
        return  response       
            




class ModbusClass():
    def __init__(self,address = '127.0.0.1', port=502 , unitId = 1, framer = None):
        self.address_ = address
        self.port_ = port
        self.uintId_ = unitId
        self.framer_ = framer

    def getConnection(self):
        if self.framer_ == None:
            return ModbusTcpClient(self.address_,port=self.port_) # retries=3, retry_on_empty=True
        else:
            return ModbusClient(self.address_, port=self.port_,framer=ModbusRtuFramer ) # retries=3, retry_on_empty=True

    def readString(self,uid,length=32,unidId = None,FC=4):
        if(unidId == None):
            unidId=self.uintId_
        result =''
        client = self.getConnection()
        numberRegisters = length/2
        try:
            if FC==3:
                rr = client.read_holding_registers(uid,numberRegisters,unit=unidId)
            else:
                rr = client.read_input_registers(uid ,numberRegisters,unit=unidId) 
            if isinstance(rr, ExceptionResponse):
                result = str(-rr.exception_code)
            elif isinstance(rr, ModbusResponse):
                result =''
                for i in range(numberRegisters):
                    result += chr(rr.registers[i]/0x100)
                    result += chr(rr.registers[i]&0xff)
                #print str(rr),rr.function_code,rr.unit_id,rr.registers
            client.close()
            return result
        except:
            return -1
            
    def readDate(self,uid,unidId = None,FC=4):
        if(unidId == None):
            unidId=self.uintId_
        value = -1
        client = self.getConnection()
        numberRegisters = 2

        try:
            if FC==3:
                rr = client.read_holding_registers(uid,numberRegisters,unit=unidId)
            else:
                rr = client.read_input_registers(uid ,numberRegisters,unit=unidId)

            if isinstance(rr, ExceptionResponse):
                value = -rr.exception_code
            elif isinstance(rr, ModbusResponse):
                value = 0
                for i in range(numberRegisters):
                    value =value * 0x10000 + rr.registers[i]  
                #print str(rr),rr.function_code,rr.unit_id,rr.registers
            client.close()
            if value > 0 :
                return datetime.datetime.utcfromtimestamp(int(value)).strftime('%Y-%m-%d %H:%M:%S')
            else:
                return value
        except:
            return -1

    def readFloat(self,uid,unidId = None,FC=4):
        if(unidId == None):
            unidId=self.uintId_
        value = -1
        client = self.getConnection()
        client.timeout=10
        client.connect()
        numberRegisters = 2
        #try:
        if True:
            if FC==3:
                rr = client.read_holding_registers(uid,numberRegisters,unit=unidId)
            else:
                rr = client.read_input_registers(uid ,numberRegisters,unit=unidId) 
            if isinstance(rr, ExceptionResponse):
                value = -rr.exception_code
            elif isinstance(rr, ModbusResponse):
                A = (rr.registers[0]/0x100)&0xFF
                B = (rr.registers[0])&0xFF
                C = (rr.registers[1]/0x100)&0xFF
                D = (rr.registers[1])&0xFF
                val = struct.pack('BBBB', D,C,B,A)
                value = struct.unpack('f',val)
            client.close()
            return value[0]
        #except:
        #    return -1
            
    def readUint32(self,uid,unidId = None,FC=4):
        if(unidId == None):
            unidId=self.uintId_
        value = -1
        client = self.getConnection()
        numberRegisters = 2
        try:
            if FC==3:
                rr = client.read_holding_registers(uid,numberRegisters,unit=unidId)
            else:
                rr = client.read_input_registers(uid ,numberRegisters,unit=unidId) 
            if isinstance(rr, ExceptionResponse):
                value = -rr.exception_code
            elif isinstance(rr, ModbusResponse):
                A = (rr.registers[0]/0x100)&0xFF
                B = (rr.registers[0])&0xFF
                C = (rr.registers[1]/0x100)&0xFF
                D = (rr.registers[1])&0xFF
                #print '<',(A,B,C,D)
                val = struct.pack('BBBB', A,B,C,D)
                value = struct.unpack('>L',val)
                #print str(rr),rr.function_code,rr.unit_id,rr.registers
            client.close()
            return value[0]
        except:
            return -1
            
    def writeUint32(self,uid,value,unidId = None):
        if(unidId == None):
            unidId=self.uintId_
        
        client = self.getConnection()
        
        u32 = struct.pack('>L',value)
        (A,B,C,D) = struct.unpack('BBBB', u32)
        #print '>',(A,B,C,D)
        r1=A*0x100+B
        r2=C*0x100+D
        
        rq=client.write_registers(uid, [r1,r2],unit=unidId)
        client.close()
        
    def writeFloat(self,uid,value,unidId = None):
        if(unidId == None):
            unidId=self.uintId_
        
        client = self.getConnection()
        
        f = struct.pack('f',value)
        (A,B,C,D) = struct.unpack('BBBB', f)
        #print '>',(A,B,C,D)
        r1=A*0x100+B
        r2=C*0x100+D
        
        rq=client.write_registers(uid, [r1,r2],unit=unidId)
        client.close()
        
        
          
if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--ip',action='store', default='10.200.4.101')
    parser.add_argument('--port',action='store', default=5042)
    parser.add_argument('--unidId',action='store', default=1)

    args = parser.parse_args()

    
    mb = ModbusClass(address = '10.200.4.101', unitId=1, port=5042 , framer = ModbusRtuFramer)

    unidId = args.unidId
    val = mb.readFloat(32,unidId=1,FC=4)
    print 'float:',val



   
